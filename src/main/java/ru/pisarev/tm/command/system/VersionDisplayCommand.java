package ru.pisarev.tm.command.system;

import ru.pisarev.tm.command.AbstractCommand;

public class VersionDisplayCommand extends AbstractCommand {
    @Override
    public String name() {
        return "version";
    }

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
    }
}
