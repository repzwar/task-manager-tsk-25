package ru.pisarev.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.entity.IWBS;
import ru.pisarev.tm.enumerated.Status;

import java.util.Date;

@Setter
@Getter
public class Task extends AbstractBusinessEntity implements IWBS {

    @NotNull
    private String name;

    @Nullable
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @NotNull
    private Date created = new Date();


    @NotNull
    public Task(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public Task(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return id + ": " + name;
    }

}
