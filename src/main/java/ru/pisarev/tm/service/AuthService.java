package ru.pisarev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.service.IUserService;
import ru.pisarev.tm.enumerated.Role;
import ru.pisarev.tm.exception.empty.EmptyLoginException;
import ru.pisarev.tm.exception.empty.EmptyPasswordException;
import ru.pisarev.tm.exception.system.AccessDeniedException;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.HashUtil;

import java.util.Optional;

public class AuthService implements ru.pisarev.tm.api.service.IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    @NotNull
    public AuthService(@NotNull IUserService userService, @NotNull final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService=propertyService;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        @NotNull final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void checkRoles(@Nullable Role... roles) {
        if (roles == null || roles.length == 0) return;
        @NotNull final User user = Optional.ofNullable(userService.findById(userId))
                .orElseThrow(AccessDeniedException::new);
        @NotNull final Role role = Optional.of(user.getRole())
                .orElseThrow(AccessDeniedException::new);
        for (@Nullable final Role item : roles) {
            if (role.equals(item)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = Optional.ofNullable(userService.findByLogin(login))
                .orElseThrow(AccessDeniedException::new);
        @NotNull final String hash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(AccessDeniedException::new);
        if (!hash.equals(user.getPasswordHash()) || user.isLocked()) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        userService.add(login, password, email);
    }
}
