package ru.pisarev.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    String getApplicationVersion();

}
