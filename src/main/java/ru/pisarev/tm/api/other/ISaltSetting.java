package ru.pisarev.tm.api.other;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.IPropertyService;

public interface ISaltSetting {

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

}
