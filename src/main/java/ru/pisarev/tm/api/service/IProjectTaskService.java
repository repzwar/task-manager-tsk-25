package ru.pisarev.tm.api.service;

import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String userId, final String projectId);

    Task bindTaskById(String userId, final String taskId, final String projectId);

    Task unbindTaskById(String userId, final String taskId);

    Project removeProjectById(String userId, final String projectId);

    Project removeProjectByIndex(String userId, final Integer index);

    Project removeProjectByName(String userId, final String name);

}
